#include <stdlib.h>

#define N 2048

double A[N][N], B[N][N], C[N][N];

int main() {
	/* random value initialization */
	srand(1);
	double rand_max_inv = 1.0 / (double)RAND_MAX;

	/* matrix initialization */
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			A[i][j] = rand() * rand_max_inv;
			B[i][j] = rand() * rand_max_inv;
			C[i][j] = 0.0;
		}
	}

	/* matrix production */
	for (int i = 0; i < N; i++) {
		for (int k = 0; k < N; k++) {
			double Aik = A[i][k];
			for (int j = 0; j < N; j++) {
				C[i][j] += Aik * B[k][j];
			}
		}
	}

	return 0;
}

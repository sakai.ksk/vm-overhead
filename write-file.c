#include <stdio.h>

#define N 10000

int main() {
	for (int i = 0; i < N; i++) {
		/* name */
		char name[32];
		snprintf(name, 30, "out/%d.txt", i);

		/* open */
		FILE *file = fopen(name, "w");

		/* write */
		for (int j = 0; j < 1024; j++) {
			fprintf(file, "%d\n", i);
		}

		/* close */
		fclose(file);
	}

	return 0;
}
